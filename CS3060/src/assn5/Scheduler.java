package assn5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Scheduler {

	
	
	public static void main(String[] args) {
		
		File inputFile = null;
		Scanner sc = null;
		
		ArrayList<Integer> processStartTime = new ArrayList<>();
		ArrayList<Integer> burstTimes = new ArrayList<>();
		
		if(0 < args.length)
		{
			inputFile = new File(args[0]);
		}
		else
		{
			System.err.println("Invalid amount of arguments: " + args.length);
		}
	
		try {
			sc = new Scanner(inputFile);
			
			while(sc.hasNextLine())
			{
				processStartTime.add(sc.nextInt());
				burstTimes.add(sc.nextInt());
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}

	public void firstComeFirstServed()
	{
		
	}
	
	public void shortestJobfirst()
	{
		
	}
	
	public void shortestRemainingTimeFirst()
	{
		
	}
	
	public void roundRobin()
	{
		
	}
	
}
