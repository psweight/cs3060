/* Promise of Originality
I promise that this source code file has, in it's entirety, been written by myself and by no other person or persons.
If at any time an exact copy of this source code is found to be used by another person in this term,
I understand that both myself and the student that submitted the copy will receive a zero on this assignment.

Preston Weight
April 2016
*/

package assn6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Assn6 {

	public static void main(String[] args) {
		
		File inputFile = null;
		Scanner sc = null;
		ArrayList<Integer> diskLocations = new ArrayList<>();
		
		if (args.length == 1) {
			inputFile = new File(args[0]);
	
			try {
				sc = new Scanner(inputFile);
				while (sc.hasNextInt()) {
						diskLocations.add(sc.nextInt());
				}
				sc.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				
			}
		} else if (args.length > 0) {
			for(int i = 0; i < args.length; i++) {
				try {
					diskLocations.add(Integer.parseInt(args[i]));
				} catch (NumberFormatException e) {
					System.err.println("Please enter a set of numbers.");
					System.exit(-1);
				}
			}
		} else {
			
			System.err.println("Invalid amount of arguments: " + args.length);
			System.exit(-1);
		}

		
		
		System.out.println("Assignment 6: Block Access Algorithms.");
		System.out.println("By: Preston Weight\n");
		
		firstComeFirstServed(copyToArrayList(diskLocations));
		shortestSeekTimeFirst(copyToArrayList(diskLocations));
		nonCircularLook(copyToArrayList(diskLocations));
		circularLook(copyToArrayList(diskLocations));
		
	}
	
	private static ArrayList<Integer> copyToArrayList(ArrayList<Integer> copyArrayList) {
		ArrayList<Integer> newArrayList = new ArrayList<Integer>(copyArrayList);
		return newArrayList;
	}
	
	static void firstComeFirstServed(ArrayList<Integer> diskData) {
		int totalSeek = 0;
				
		for(int x = 0; x < diskData.size(); x++) {
		
			int currentPos = diskData.get(x);
			
			if((x + 1) < diskData.size()) {
				int nextPos = diskData.get(x + 1);
				totalSeek += Math.abs(currentPos - nextPos);
			}
		}
		
		System.out.println("FCFS Total Seek: \t" + totalSeek);
	}
		
	static void shortestSeekTimeFirst(ArrayList<Integer> diskData) {
		int totalSeek = 0;
		int currentPosition = 0;
		int nextPosition = 0;
		
	
		for(int i = 0; i < diskData.size() - 1; i++) {
			
			int smallestDiff = 100000000;
			
			for(int j = i; j < diskData.size(); j++) {
				int diff = Math.abs(diskData.get(currentPosition) - diskData.get(j));
				if(diff != 0 && diff < smallestDiff ) {
					smallestDiff = diff;
					nextPosition = j;
				}
			}
				
				totalSeek += smallestDiff;
				currentPosition++;
				diskData.add(currentPosition, diskData.remove(nextPosition));
										
		}
		
		System.out.println("SSTF Total Seek: \t" + totalSeek);	
	}
	
	static void nonCircularLook(ArrayList<Integer> diskData) {
		int totalSeek = 0;
		int currentPosition = 0;
		int nextPosition = 0;
		
		int min = getMin(diskData);
		int max = getMax(diskData);
		boolean maxFlag = false;
		
		for(int i = 0; i < diskData.size() - 1; i++) {
			
			int smallestDiff = 100000000;
			
			for(int j = i; j < diskData.size(); j++) {
				int diff = diskData.get(currentPosition) - diskData.get(j);
				
				if(diskData.get(currentPosition) == max) {
					maxFlag = true;
				}
				
				// Know it is in the positive direction
				if(diff < 0 && maxFlag != true) {
					diff = Math.abs(diskData.get(currentPosition) - diskData.get(j));
					if(diff != 0 && diff < smallestDiff ) {
						smallestDiff = diff;
						nextPosition = j;
					}
				}
				
				//Switch direction when maxflag is hit
				if(diff > 0 && maxFlag != false) {
					diff = Math.abs(diskData.get(currentPosition) - diskData.get(j));
					if(diff != 0 && diff < smallestDiff ) {
						smallestDiff = diff;
						nextPosition = j;
					}
				}
			}
				totalSeek += smallestDiff;
				currentPosition++;
				diskData.add(currentPosition, diskData.remove(nextPosition));					
		}	

		System.out.println("LOOK Total Seek: \t" + totalSeek);
	}
	
	// just need to finish implementing for lower half
	static void circularLook(ArrayList<Integer> diskData) {
		int totalSeek = 0;
		int currentPosition = 0;
		int nextPosition = 0;
		
		int min = getMin(diskData);
		int max = getMax(diskData);
		boolean maxFlag = false;
		
		for(int i = 0; i < diskData.size() - 1; i++) {
			
			int smallestDiff = 100000000;
			int greatestDiff = -1;
			
			for(int j = i; j < diskData.size(); j++) {
				int diff = diskData.get(currentPosition) - diskData.get(j);
				
				if(diskData.get(currentPosition) == max) {
					maxFlag = true;
					diff = diskData.get(currentPosition) - diskData.get(j);
				}
				
				// Know it is in the positive direction
				if(diff < 0 && maxFlag != true) {
					diff = Math.abs(diskData.get(currentPosition) - diskData.get(j));
					if(diff != 0 && diff < smallestDiff ) {
						smallestDiff = diff;
						nextPosition = j;
					}
				}
				
				if(diff > 0 && maxFlag != false) {
					diff = Math.abs(diskData.get(currentPosition) - diskData.get(j));
					if(diff != 0 && diff > greatestDiff ) {
						greatestDiff = diff;
						nextPosition = j;
					}
				}
			}
		
				if(maxFlag != true) {
					totalSeek += smallestDiff;
				} else {
					totalSeek += greatestDiff;
					maxFlag = false;
				}
					
					currentPosition++;
					diskData.add(currentPosition, diskData.remove(nextPosition));									
		}

		System.out.println("C-LOOK Total Seek: \t" + totalSeek);
	}
	
	static int getMin(ArrayList<Integer> list) {
		int minValue = list.get(0);
		
		for(int i = 1; i < list.size(); i++) {
			if(list.get(i) < minValue) {
				minValue = list.get(i);
			}
		}		
		return minValue;
		
	}
	
	static int getMax(ArrayList<Integer> list) {
		int maxValue = list.get(0);
		
		for(int i = 1; i < list.size(); i++) {
			if(list.get(i) > maxValue) {
				maxValue = list.get(i);
			}
		}	
		return maxValue;
	}

}
